package com.roadmobo.roadmobo.LayoutActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Location;
import android.net.Uri;
import android.os.Debug;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.entire.sammalik.samlocationandgeocoding.SamLocationRequestService;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nineoldandroids.animation.Animator;
import com.roadmobo.roadmobo.MainActivity;
import com.roadmobo.roadmobo.Model.AppSharedPreference;
import com.roadmobo.roadmobo.Model.Config;
import com.roadmobo.roadmobo.Model.RoadmoboReport;
import com.roadmobo.roadmobo.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import info.abdolahi.CircularMusicProgressBar;

/**
 * Created by AlZihad on 1/16/2017.
 */
public class AddReportActivity extends Fragment
{
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    ImageView image_gallery, image_camera,report_image;
    Spinner report_type;
    Button report_send;
    EditText report_desrip;
    Bitmap image;
    String report_date,report_time, user_id;
    Location userLocation;
    CircularMusicProgressBar circularMusicProgressBar;
    LinearLayout report_section;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.add_report_layout, container, false);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        inItContents(v);
        Firebase.setAndroidContext(getContext());

        image_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                galleryIntent();
            }
        });
        image_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraIntent();
            }
        });

        report_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitReport();
            }
        });


        new SamLocationRequestService(getActivity()).executeService(new SamLocationRequestService.SamLocationListener() {
            @Override
            public void onLocationUpdate(Location location, Address address)
            {
                if (location!=null) {
                    Toast.makeText(getContext(), "mapped: " + location.toString(), Toast.LENGTH_SHORT).show();
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    userLocation = location;
                }
            }
        });
        return v;
    }

    void inItContents(View v)
    {
        report_section = (LinearLayout) v.findViewById(R.id.report_section);
        image_camera = (ImageView) v.findViewById(R.id.image_camera);
        image_gallery = (ImageView) v.findViewById(R.id.image_gallery);
        report_image = (ImageView) v.findViewById(R.id.report_image);
        report_type = (Spinner) v.findViewById(R.id.report_type);
        report_send =(Button) v.findViewById(R.id.report_send);
        report_desrip = (EditText) v.findViewById(R.id.report_descrip);
        circularMusicProgressBar = (CircularMusicProgressBar) v.findViewById(R.id.circluar_progress);
        circularMusicProgressBar.setVisibility(View.GONE);

    }

    public void submitReport()
    {
        if (image != null && report_desrip.getText().toString().length()>=10 && userLocation != null)
        {
            circularMusicProgressBar.setVisibility(View.VISIBLE);
            circularMusicProgressBar.setImageBitmap(image);
            circularMusicProgressBar.setValue(0);
            report_section.setVisibility(View.GONE);
            Timer timer = new Timer();
            timer.schedule(new SwapPlayContent(circularMusicProgressBar), 1600);

            uploadImage(image);
        }
        else
        {

        }
    }

    public void uploadImage(Bitmap bitmap)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        AppSharedPreference asp = AppSharedPreference.getInstance(getContext());
        Calendar c = Calendar.getInstance();
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat time_format = new SimpleDateFormat("HH:mm:ss");

        user_id = asp.getUserId();
        report_date  = date_format.format(c.getTime());
        report_time = time_format.format(c.getTime());

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl(Config.firebaseStorageLink);
        StorageReference imagesRef = storageRef.child("Report_image/"+user_id+"_"+report_date+"_"+report_time);

        UploadTask uploadTask = imagesRef.putBytes(data);

        uploadTask.addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception exception)
            {
                /*FragmentManager fm = getFragmentManager();
                RetryDialog dialogFragment = new RetryDialog ();
                dialogFragment.show(fm, "Sample Fragment");*/
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>()
        {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot)
            {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                circularMusicProgressBar.setValue((float) progress);
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
               // Toast.makeText(getContext(), downloadUrl.toString(), Toast.LENGTH_LONG).show();
               // Log.d("URI: ", downloadUrl.toString());
                String latitude = userLocation.getLatitude()+"";
                String longitude = userLocation.getLongitude()+"";
                String description = report_desrip.getText().toString();
                String type = report_type.getSelectedItem().toString();
                // upload data to firebase
                RoadmoboReport roadmoboReport = new RoadmoboReport(user_id,report_time,report_date,latitude,longitude,description,type,downloadUrl.toString());
                Firebase ref = new Firebase(Config.firebaseLink);
                Firebase newRef = ref.child("RoadmoboReport").push();
                //newRef.setValue("RoadmoboReport");
                newRef.setValue(roadmoboReport ,new Firebase.CompletionListener()
                {
                    @Override
                    public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                        if (firebaseError != null)
                        {
                            Toast.makeText(getContext(), "Data could not be saved. ", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(getContext(), R.string.upload_complete_dialog_desc, Toast.LENGTH_LONG).show();
                            circularMusicProgressBar.setVisibility(View.GONE);
                            report_section.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        });
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        image = thumbnail;
        report_image.setImageBitmap(thumbnail);
        //submitCrackImage(thumbnail);
        // ivImage.setImageBitmap(thumbnail);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), data.getData());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        image = bm;
        report_image.setImageBitmap(bm);

    }

    public class SwapPlayContent extends TimerTask
    {
        CircularMusicProgressBar play;
        SwapPlayContent(CircularMusicProgressBar circularMusicProgressBar)
        {
            this.play = circularMusicProgressBar;
        }
        @Override
        public void run()
        {
            try
            {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run()
                    {
                        YoYo.with(Techniques.Pulse).duration(800).playOn(play);
                        Timer timer = new Timer();
                        timer.schedule(new SwapPlayContent(play), 1600);
                    }
                });
                this.cancel();
            }
            catch (Exception e)
            {
                this.cancel();
            }

        }
    }


}
