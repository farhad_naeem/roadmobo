package com.roadmobo.roadmobo.LayoutActivity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.roadmobo.roadmobo.Adapters.SlidingTabLayout;
import com.roadmobo.roadmobo.Adapters.ViewPagerAdapter;
import com.roadmobo.roadmobo.R;

/**
 * Created by AlZihad on 2/9/2017.
 */

public class Home extends Fragment
{
    public ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    int Numboftabs =2;
    CharSequence Titles[]={"চারপাশের ঘটনা","আমার রিপোর্ট "};
    ViewPager pager;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.content_home, container, false);
        adapter =  new ViewPagerAdapter(getActivity().getSupportFragmentManager(),Titles,Numboftabs);
        pager = (ViewPager) v.findViewById(R.id.pager);
        pager.setAdapter(adapter);
        tabs = (SlidingTabLayout) v.findViewById(R.id.slidingtab);
        tabs.setDistributeEvenly(true);
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabColor);
            }
        });
        tabs.setViewPager(pager);
        return v;
    }
}
