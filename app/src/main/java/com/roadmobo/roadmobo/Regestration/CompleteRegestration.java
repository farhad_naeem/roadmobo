package com.roadmobo.roadmobo.Regestration;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.roadmobo.roadmobo.MainActivity;
import com.roadmobo.roadmobo.Model.Config;
import com.roadmobo.roadmobo.Model.RoadmoboUser;
import com.roadmobo.roadmobo.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class CompleteRegestration extends AppCompatActivity {

    EditText reg_name, reg_email, reg_mobile, reg_dob,reg_gender;
   // Profile userProfile;
    ImageView avatar;
    Button reg_save;
    String id, name, email, mobile, dob, gender;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_complete_regestration);
        Firebase.setAndroidContext(this);

        inItAll();

        /*ProfileTracker profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile)
            {
                userProfile = currentProfile;
            }
        };
        userProfile = Profile.getCurrentProfile();*/

        setProfileValues();

        reg_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if (reg_mobile.getText().toString().length()>=7 && reg_dob.getText().toString().length()==4)
                {
                    mobile = reg_mobile.getText().toString();
                    dob = reg_dob.getText().toString();
                    name = reg_name.getText().toString();
                    email = reg_email.getText().toString();
                    gender = reg_gender.getText().toString();
                    RoadmoboUser user = new RoadmoboUser(id,  name,  mobile,  email,  dob,  gender);

                    Firebase ref = new Firebase(Config.firebaseLink);
                    Firebase newRef = ref.child("RoadmoboUser").push();

                    newRef.setValue(user ,new Firebase.CompletionListener()
                    {
                        @Override
                        public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                            if (firebaseError != null)
                            {
                               // System.out.println("You are alrady Regestered!!!. " + firebaseError.getMessage());
                                Toast.makeText(getApplicationContext(),"Hello! Welcome Back!!!",Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            }
                            else
                            {
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            }
                        }
                    });
                }
            }
        });
    }

    void inItAll()
    {
        reg_name = (EditText) findViewById(R.id.reg_name);
        reg_email = (EditText) findViewById(R.id.reg_email);
        reg_mobile = (EditText) findViewById(R.id.reg_mobile);
        reg_dob = (EditText) findViewById(R.id.reg_dob);
        reg_gender = (EditText) findViewById(R.id.reg_gender);
        avatar = (ImageView) findViewById(R.id.avatar);
        reg_save = (Button) findViewById(R.id.reg_save);
    }
    void setProfileValues()
    {
        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response)
                    {
                        Log.d("login",object.toString());
                        Log.d("login",response.toString());
                        try {
                            reg_email.setText(object.getString("email"));
                            reg_name.setText(object.getString("name"));
                            reg_gender.setText(response.getJSONObject().getString("gender"));
                            reg_gender.setFocusable(false);
                            reg_gender.setEnabled(false);
                            reg_email.setFocusable(false);
                            reg_email.setEnabled(false);
                            reg_name.setFocusable(false);
                            reg_name.setEnabled(false);
                            String userID = object.getString("id");
                            id = userID;
                            Picasso.with(getApplicationContext()).load("https://graph.facebook.com/" + userID + "/picture?type=large").into(avatar);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, name, email,gender, birthday, location");
        request.setParameters(parameters);
        request.executeAsync();
    }
}
