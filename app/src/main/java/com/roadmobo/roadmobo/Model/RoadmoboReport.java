package com.roadmobo.roadmobo.Model;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by AlZihad on 1/18/2017.
 */
public class RoadmoboReport
{
    String user_id, time, date, latitude, longitude, description, type , report_image_link;
    public RoadmoboReport()
    {

    }
    //Uri report_image_link;
    public RoadmoboReport(String user_id,String time,String date,String latitude,String longitude,String description,String type,String report_image_link)
    {
        this.user_id = user_id;
        this.date= date;
        this.time = time;
        this.latitude = latitude;
        this.longitude=longitude;
        this.description = description;
        this.type = type;
        this.report_image_link = report_image_link;
    }

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getReport_image_link() {
        return report_image_link;
    }

    public String getTime() {
        return time;
    }

    public String getType() {
        return type;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setReport_image_link(String report_image_link) {
        this.report_image_link = report_image_link;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
