package com.roadmobo.roadmobo.Model;

/**
 * Created by AlZihad on 1/17/2017.
 */
public class Config
{
        public static final String firebaseLink = "https://roadmobo-4dd2b.firebaseio.com/";
        public static final String firebaseStorageLink = "gs://roadmobo-4dd2b.appspot.com";//
        public static final String user_id = "USER_ID";//
}
