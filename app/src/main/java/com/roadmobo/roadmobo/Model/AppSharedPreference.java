package com.roadmobo.roadmobo.Model;

/**
 * Created by AlZihad on 1/18/2017.
 */


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class AppSharedPreference {
    private static AppSharedPreference mAppSharedPreference;

    private SharedPreferences mSharedPreferences;


    private Editor mEditor;
    private static Context mContext;

    /*
     * Implementing Singleton DP
     */
    private AppSharedPreference()
    {
        mSharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(mContext);
        mEditor = mSharedPreferences.edit();
    }

    public static AppSharedPreference getInstance(Context context)
    {
        mContext = context;
        if (mAppSharedPreference == null)
            mAppSharedPreference = new AppSharedPreference();
        return mAppSharedPreference;
    }
    public String getUserId()
    {
        return mSharedPreferences.getString(Config.user_id,"null");
    }
    public void setUserId(String s)
    {
        mEditor.putString(Config.user_id,s);
        mEditor.commit();
        //return mSharedPreferences.getString(Constant.EmergencyNumber1,"null");
    }
}