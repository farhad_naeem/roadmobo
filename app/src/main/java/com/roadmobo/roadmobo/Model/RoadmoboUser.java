package com.roadmobo.roadmobo.Model;

/**
 * Created by AlZihad on 1/17/2017.
 */
public class RoadmoboUser
{
    String id,  name,  mobile,  email,  dob,  gender;
    public RoadmoboUser(String id, String name, String mobile, String email, String dob, String gender)
    {
        this.dob = dob;
        this.email = email;
        this.gender = gender;
        this.id = id;
        this.mobile = mobile;
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public String getId() {
        return id;
    }

    public String getDob() {
        return dob;
    }

    public String getGender() {
        return gender;
    }

    public String getMobile() {
        return mobile;
    }

    public String getName() {
        return name;
    }

}
