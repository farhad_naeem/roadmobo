package com.roadmobo.roadmobo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.roadmobo.roadmobo.Adapters.SlidingTabLayout;
import com.roadmobo.roadmobo.Adapters.ViewPagerAdapter;
import com.roadmobo.roadmobo.LayoutActivity.Home;
import com.roadmobo.roadmobo.Model.AppSharedPreference;
import com.roadmobo.roadmobo.Model.RoadmoboReport;
import com.roadmobo.roadmobo.Services.NetworkStateReceiver;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import cn.carbs.android.library.MDDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MaterialTabListener, NetworkStateReceiver.NetworkStateReceiverListener
{


    ImageView avatar;
    TextView nav_user_name;
    AlertDialog.Builder alertDialogBuilder;
    AlertDialog alertDialog;
    private NetworkStateReceiver networkStateReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // tabbed view
        /**/
        // enabling location manager
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        // GUI initialize
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().logOut();
                startActivity(new Intent(getApplicationContext(),SplashScreen.class));
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        displaySelectedScreen(R.id.nav_home);

        View hView =  navigationView.getHeaderView(0);
        avatar = (ImageView) hView.findViewById(R.id.nav_imageView);
        nav_user_name = (TextView) hView.findViewById(R.id.nav_user_name);

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);

        // Check network availablility
        if (isOnline())
        {
            inItUserData();
            if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) )
            {
                showGPSSettings();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Network not available",Toast.LENGTH_LONG).show();
        }
    }

    public boolean isOnline()
    {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    public void  showGPSSettings()
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AppTheme));
        // Setting Dialog Title
        alertDialog.setTitle(R.string.gps_not_avail_dialog_head);
        // Setting Dialog Message
        alertDialog.setMessage(R.string.gps_not_avail_dialog_desc);

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);

        // On pressing Settings button
        alertDialog.setPositiveButton(R.string.gps_not_avail_dialog_positive, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton(R.string.gps_not_avail_dialog_negative, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    public void showToast(final RoadmoboReport roadmoboReport)
    {
        new MDDialog.Builder(MainActivity.this)
//              .setContentView(customizedView)
                .setContentView(R.layout.view_report_layout)
                .setContentViewOperator(new MDDialog.ContentViewOperator() {
                    @Override
                    public void operate(View contentView)
                    {
                        ImageView reportImage= (ImageView) contentView.findViewById(R.id.report_image);
                        TextView report_time= (TextView) contentView.findViewById(R.id.report_time);
                        CircleImageView circleImageView = (CircleImageView) contentView.findViewById(R.id.reporter_pro_pic);
                        Picasso.with(getApplicationContext()).load("https://graph.facebook.com/" + roadmoboReport.getUser_id() + "/picture?type=large").into(circleImageView);
                        Picasso.with(getApplicationContext())
                                .load(roadmoboReport.getReport_image_link())
                                .placeholder( R.drawable.loading)
                                .into(reportImage/*, new Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError()
                                    {
                                        Toast.makeText(getApplicationContext(),"error in loading",Toast.LENGTH_LONG).show();
                                    }
                                }*/);
                        report_time.setText(roadmoboReport.getDate()+" "+roadmoboReport.getTime());
                    }
                })
                .setTitle(roadmoboReport.getDescription())
                .setNegativeButton("Back",new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                    }
                })

                .setPositiveButton("okay",new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                })
                .setPositiveButtonMultiListener(new MDDialog.OnMultiClickListener() {
                    @Override
                    public void onClick(View clickedView, View contentView) {
                       // EditText et = (EditText) contentView.findViewById(R.id.edit0);
                      //  Toast.makeText(getApplicationContext(), "edittext 0 : " + et.getText(), Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButtonMultiListener(new MDDialog.OnMultiClickListener() {
                    @Override
                    public void onClick(View clickedView, View contentView)
                    {
                       // EditText et = (EditText) contentView.findViewById(R.id.edit1);
                       // Toast.makeText(getApplicationContext(), "edittext 1 : " + et.getText(), Toast.LENGTH_SHORT).show();
                    }
                })
                .setWidthMaxDp(600)
//              .setShowTitle(false)//default is true
//              .setShowButtons(true)//default is true
                .create()
                .show();
    }

    void inItUserData()
    {
        final Profile userProfile = Profile.getCurrentProfile();

        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response)
                    {
                        try
                        {
                            String id = object.getString("id");
                            AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                            asp.setUserId(id);

                            Picasso.with(getApplicationContext()).load("https://graph.facebook.com/" + id + "/picture?type=large").into(avatar);
                            nav_user_name.setText(userProfile.getName());

                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        displaySelectedScreen(id);
        return true;

        /*Fragment fragment = null;
        if (id == R.id.nav_camera)
        {
            // Handle the camera action
        }
        else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send)
        {

        }

        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);*/
    }

    private void displaySelectedScreen(int itemId) {

        //creating fragment object
        Fragment fragment = null;

        //initializing the fragment object which is selected
        switch (itemId)
        {
            case R.id.nav_home:
                fragment = new Home();
                break;
        }

        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onTabSelected(MaterialTab tab) {

    }

    @Override
    public void onTabReselected(MaterialTab tab) {

    }

    @Override
    public void onTabUnselected(MaterialTab tab) {

    }

    @Override
    public void networkAvailable()
    {
        if (alertDialog!= null)
        {
            alertDialog.cancel();
        }
        Toast.makeText(getApplicationContext(),"Network state chnaged to  available",Toast.LENGTH_LONG).show();
    }

    @Override
    public void networkUnavailable()
    {
        Toast.makeText(getApplicationContext(),"Network state chnaged to  not available",Toast.LENGTH_LONG).show();
        showNetNotAvailableDialogue();
    }

    public void showNetNotAvailableDialogue()
    {

        // set title
        alertDialogBuilder.setTitle(R.string.net_not_avail_dialog_head);
        // set dialog message
        alertDialogBuilder
                .setMessage(R.string.net_not_avail_dialog_desc)
                .setCancelable(false)
                .setPositiveButton(R.string.net_not_avail_dialog_positive,new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog,int id)
                    {
                        dialog.cancel();
                    }
                })
                .setNegativeButton(R.string.net_not_avail_dialog_negative,new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog
        alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

}
